﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RESTServicesCRUD
{
    [DataContract]
    public class Employee
    {
        [DataMember]
        public int EmployeeId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public decimal Salary { get; set; }
        [DataMember]
        public string Departmentect { get; set; }
    }
    public interface IEmployeeRepository
    {
        List<Employee> GetAllEmployee();
        Employee GetEmployyByName(string name);
        Employee AddNewEmployee(Employee emp);
      
    }
    public class EmployeeRepository : IEmployeeRepository
    {
        public EmployeeRepository()
        {
            AddNewEmployee(new Employee { Name = "Hung", Salary = 1000000, Departmentect ="T1702M"});
            AddNewEmployee(new Employee { Name = "Dung", Salary = 2000000, Departmentect = "T1702M" });
            AddNewEmployee(new Employee { Name = "Tung", Salary = 3000000, Departmentect = "T1702M" });
        }
        private List<Employee> listEmployee = new List<Employee>();
        private int counter = 1;
        public Employee AddNewEmployee(Employee emp)
        {
            if (emp == null)
            {
                throw new ArgumentNullException("newEmployee");
            }
            emp.EmployeeId = counter++;
            listEmployee.Add(emp);
            return emp;
        }

        public List<Employee> GetAllEmployee()
        {
            return listEmployee;
        }

        public Employee GetEmployyByName(string name)
        {
            return listEmployee.Find(b => b.Name == name);
        }


        }
    }
