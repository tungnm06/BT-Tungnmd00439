﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RESTServicesCRUD
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEmployeeServices" in both code and config file together.
    [ServiceContract]
    public interface IEmployeeServices
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,

        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "Employee/")]
        List<Employee> GetEmployeeList();
        [OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,

        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "Employee/{name}")]
        Employee GetEmployeeByName(string name);
        [OperationContract]
        [WebInvoke(Method = "POST",
      ResponseFormat = WebMessageFormat.Json,
      RequestFormat = WebMessageFormat.Json,

      BodyStyle = WebMessageBodyStyle.Bare,
      UriTemplate = "AddEmployee/")]
        string AddEmployee(Employee emp);
    }
}
