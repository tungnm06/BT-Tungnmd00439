﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace RESTServicesCRUD
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeServices" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeServices.svc or EmployeeServices.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeServices : IEmployeeServices
    {
        static IEmployeeRepository repository = new EmployeeRepository();

        public string AddEmployee(Employee emp)
        {
            Employee newEmployee = repository.AddNewEmployee(emp);
            return "id =" + emp.EmployeeId;
        }

        public Employee GetEmployeeByName(string name)
        {
            return repository.GetEmployyByName(name);
        }

        public List<Employee> GetEmployeeList()
        {
            return repository.GetAllEmployee();
        }
    }
}
