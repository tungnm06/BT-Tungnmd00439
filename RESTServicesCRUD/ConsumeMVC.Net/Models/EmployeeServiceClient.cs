﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace ConsumeMVC.Net.Models
{
    public class EmployeeServiceClient
    {
        string BASE_URL = "http://localhost:64629/EmployeeServices.svc/rest/";
        public List<Employee> getAllEmployee()
        {
            var syncClient = new WebClient();
           
            var content = syncClient.DownloadString(BASE_URL + "Employee/");
            var json_serializer = new JavaScriptSerializer();
            return json_serializer.Deserialize<List<Employee>>(content);

        }
        public bool addEmployee(Employee emp)
        {

            return true;
        }

    }
}